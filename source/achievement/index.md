---
seo_title: 我的成就
cover: true
sidebar: []

---

<center>
{% span large red, 我 %}
{% span large yellow, 的 %}
{% span large green, 成 %}
{% span large blue, 就 %}
</center>
<br>

{% timeline 我的成就表 %}


{% timenode 2021年2月 %}

1.重新开张博客，更换博客框架为HEXO，主题为volantis,使用GITHUB pages进行静态托管，域名依旧用的阿里云，用的七牛云的ssl服务，同时用vercel进行托管，白嫖了mongdb数据库500M。

2.完美校园自动打卡运用验证码接口，继续帮本班同学打卡。

{% endtimenode %}


{% timenode 2020年 %}

1.完美校园自动打卡脚本失效，登录新设备必须要验证码。遂搁置。。。

{% endtimenode %}

{% timenode hut大一上学期 %}

搞副业掌握四大技能
    1.完美校园自动打卡程序
    2.学习通刷网课程序
    3.校园卡复制原理
    4.路由器破解锐捷认证

{% endtimenode %}

{% endtimeline %}




