---
title: 安卓脱壳之战-腾讯脱壳   
date: 2021-03-13 14:49:42
tags: 加固
categories: [逆向]
---
今天来个腾讯的壳子

<!-- more -->

我们今天来脱壳，和昨天一样的工具:  
反射大师  
虚拟大师  
MT管理器  
  
然后准备今天脱壳的软件程序:全力万花筒  
这是一个酷安的小伙伴开发的程序，我用过挺有意思的，但是就是有很多广告，然后是腾讯加固让我产生了想脱壳的想法！

![](http://www.codetip.net/content/uploadfile/202006/fc766e39d1d0e746f5b074c041082ea120200629025955.jpg)

可以看到是腾讯御安全加固，我们接下来和昨天一样先脱壳，我们打开虚拟大师导入并安装软件

![](http://www.codetip.net/content/uploadfile/202006/1c2e1474b5ceb53c9805ba3d47fa72e220200629025955.jpg)

接着和昨天一样，我们打开反射打开并选择这个软件，点击打开

![](http://www.codetip.net/content/uploadfile/202006/eb18279d848e2080c42df5223bcce9c620200629025955.jpg)

![](http://www.codetip.net/content/uploadfile/202006/df27631febb3a78891746ce71397fea820200629025955.jpg)

接着我们等待加载到主界面，点击中间的图形，然后和昨天一样写出dex文件

![](http://www.codetip.net/content/uploadfile/202006/e3faeb11c03086e8f4581f06393cffc520200629025955.jpg)

![](http://www.codetip.net/content/uploadfile/202006/a5106d6ad89430f662df626ebdc3b66b20200629025955.jpg)

![](http://www.codetip.net/content/uploadfile/202006/594b57017597926ac5138e74a3399ef820200629025955.jpg)

接着我们把写出的dex文件导入到本地！这时候虚拟大师就没用了，和昨天一样！然后我们在MT管理器找到软件位置VMOSfiletransferstation这个文件夹里，如果有多的分不清可以看导出时间,然后删除不需要的classes.dex文件，点击修复它

![](http://www.codetip.net/content/uploadfile/202006/52673258e41c9292fcbe9c99e415b70b20200629025956.jpg)

修复了之后把classes.dex原文件删除，把修复的classes_fix.dex重命名为classes.dex

![](http://www.codetip.net/content/uploadfile/202006/35f88fc22c4a987b102a43a4d2985aeb20200629025956.jpg)

上面的就是脱壳的教程，几乎通用大家可以记记！  
接下来就是修复教程了，大家用小本本记好！![嘿哈](http://www.codetip.net/content/uploadfile/202006/3a842de96f9aa8899c828061e7af20a320200629025956.png)  
首先我们找到加固的原安装包，点击一下选择查看

![](http://www.codetip.net/content/uploadfile/202006/37814b51e16f3fcf2d66c72789a9fb7620200629025956.jpg)

我们继续点击加固的classes.dex文件，选择dex++方式打开

![](http://www.codetip.net/content/uploadfile/202006/d82b38689cb5095f835112395318c5e820200629025956.jpg)

然后我们依次点击com/wrapper.proxyapplication

![](http://www.codetip.net/content/uploadfile/202006/9b92429058f1bb8e2ea26debb410b41120200629025956.jpg)

我们点击打开，然后复制第一个类似包名的东西

![](http://www.codetip.net/content/uploadfile/202006/530888896e89bef6fd28cfdff48e104520200629025956.jpg)

返回到主界面点击AndroidManifest.xml，选择反编译

![](http://www.codetip.net/content/uploadfile/202006/335edd0b65deb34277afd8181f6cbc1b20200629025956.jpg)

然后和360加固一样我们搜索<application替换里面的android:name这里，把我们复制的替换进去，复制前

![](http://www.codetip.net/content/uploadfile/202006/2f673bbe379d3bb7ad3d57c5dc33991720200629025956.jpg)

复制后

![](http://www.codetip.net/content/uploadfile/202006/847e3fa7940e5edcbfab81d471e44dee20200629025956.jpg)

然后点击保存返回更新，取消自动签名

![](http://www.codetip.net/content/uploadfile/202006/71efe289f4f85ff132de507d02e6be0a20200629025956.jpg)

![](http://www.codetip.net/content/uploadfile/202006/e2eebcc81517557c9e3b8a1d66d64c4520200629025956.jpg)

接下来，我们删除加固里面的classes.dex文件，取消自动签名

![](http://www.codetip.net/content/uploadfile/202006/cb26c71a37ce00834eb6a4beddbf5a2020200629025956.jpg)

然后把脱壳好的文件直接添加进来，打钩自动签名

![](http://www.codetip.net/content/uploadfile/202006/950ed6e1c51822cc22a4b600b7d6c65a20200629025956.jpg)

到主界面这里已经脱壳+修复完成了！我们点击软件看到已经是伪加固了

![](http://www.codetip.net/content/uploadfile/202006/7fb1460bce8e087672098757b827bf7220200629025956.jpg)

然后我们接下来安装测试软件

![](http://www.codetip.net/content/uploadfile/202006/12168ab948a1807d0683c03200bcf94f20200629025956.jpg)

已经看到未知加固状态，我们就完成了！  
感谢大家围观，我们明日再战其他加固壳！

本博客所有文章如无特别注明均为原创。作者：[倪风](http://www.codetip.net/author/1)  
原文地址《安卓脱壳之战-腾讯脱壳》



### 相关推荐

*   [安卓的脱壳之战-邦邦脱壳](http://www.codetip.net/post-20.html)
*   [安卓脱壳之战-360脱壳](http://www.codetip.net/post-18.html)
*   [安卓的脱壳之战-爱加密加固](http://www.codetip.net/post-21.html)
