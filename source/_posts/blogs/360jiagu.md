---
title: 安卓脱壳之战-360脱壳   
date: 2021-03-12 21:49:42
tags: [360加固,逆向,脱壳]
keywords: 360加固,脱壳
categories: [逆向]
---
今天给大家带来最新的360加固的脱壳和修复教程

<!-- more -->

需要用到的工具我会打包在我常用的链接里面，方便大家食用:
本次用到的工具有:
1.虚拟大师
2.MT管理器
3.反射大师
以及一个360过效验的文件！

好了废话不多说我们开始今天的教程:首先我们下载好360过效验的文件，然后在MT管理器找到它

![1](http://www.codetip.net/content/uploadfile/202006/74dc12d4c805c8cf9917f72fe9b49d2320200629025757.jpg)

然后我们继续准备脱壳的安装包和虚拟大师，这里我用的是一个人修改过的抖音加固版本

![2](http://www.codetip.net/content/uploadfile/202006/46896eb5cdaf419d2aa63822a6a4916520200629025757.jpg)

可以看到是360加固的抖音文件，首先我们打开虚拟大师（VMOS），然后安装好MT管理器，XPOSED框架激活

![](http://www.codetip.net/content/uploadfile/202006/f960f5c40d97a348f287d2ddbf3f65c320200629025757.jpg)

点击下面倒数第二个文件夹，把反射大师导入并安装在上面的链接就可以下载

![](http://www.codetip.net/content/uploadfile/202006/4c769f180a1140335afd106c7a2e5ab220200629025757.jpg)

点击我要导入，找到自己存放反射大师的位置，安装好之后，在XPOSED里面去打钩重启模拟器

![](http://www.codetip.net/content/uploadfile/202006/a8c2e5e6d38bdafaaddefe9a8b001ac420200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/e4ca7c256963c662920103333a14c1b620200629025757.jpg)

这时候我们在虚拟大师里面打开反射大师看看激活没有:打开提示创建测试窗口就激活了！

![](http://www.codetip.net/content/uploadfile/202006/9047eb740a27b1b4a7c13dd1a163164820200629025757.jpg)

接下来导入我们需要脱壳的抖音安装包，和上面一样在文件里面导入，我就不多演示了

![](http://www.codetip.net/content/uploadfile/202006/57125446ec873fcdd54fad19d7b0bc1720200629025757.jpg)

然后返回虚拟大师的桌面，打开反射大师，点击抖音并选择这个软件

![](http://www.codetip.net/content/uploadfile/202006/50c9f41d16355faa910c537313a2926e20200629025757.jpg)

然后会提示你打开软件我们点击打开

![](http://www.codetip.net/content/uploadfile/202006/ad6d2911fef5cf8a740f454082bbc73720200629025757.jpg)

等待它加载到抖音主界面的时候，可以看到有一个标志在中间

![](http://www.codetip.net/content/uploadfile/202006/5c2615d41f142d0bab178339fe90cee320200629025757.jpg)

然后我们点击它，选择第二个:当前activity，然后选择写出dex

![](http://www.codetip.net/content/uploadfile/202006/61554f1d2b33530390c39a20e5d37c9020200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/76ae3436688002651a2ddb7ab9fc26f920200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/8ee5d58e14ed333c58708589a9a0d61c20200629025757.jpg)

这个时候软件的壳就脱好了，这个脱壳的dex文件默认在根目录最下面，然后我们找到它，把它导出本机

![](http://www.codetip.net/content/uploadfile/202006/f8e72afaf703043395ca2d78b3363b1820200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/9dbee6d95c4299d6590c7ece3ccd6cfb20200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/d9ed92fdfdcdadfd9bf40703c9f8197c20200629025758.jpg)

等待导出完成，我们就进入重要的部分了！

![](http://www.codetip.net/content/uploadfile/202006/9dbee6d95c4299d6590c7ece3ccd6cfb20200629025757.jpg)

![](http://www.codetip.net/content/uploadfile/202006/d9ed92fdfdcdadfd9bf40703c9f8197c20200629025758.jpg)

首先在本地我们先找到脱壳的文件并修复dex  
路径:/storage/emulated/0/VMOSfiletransferstation/

![](http://www.codetip.net/content/uploadfile/202006/72f8f72e137d986f8aa456e0bb51818d20200629025758.jpg)

![](http://www.codetip.net/content/uploadfile/202006/717abef78b1742b41ee03cf25303cb7320200629025758.jpg)

修复成功后，我们删掉原文件，把修复的文件命名为classes.dex，然后点击确定

![](http://www.codetip.net/content/uploadfile/202006/d4ff4f64b8771e8b122d9cf3cf5fc6aa20200629025758.jpg)

虚拟大师到这里就没用了，我们可以结束它进程，这就不演示了，接着我们去找到本地360加固的安装包，用查看的方式打开它，保留这三个文件，删除其他不需要的文件

![](http://www.codetip.net/content/uploadfile/202006/6ad74d84d2a38bba6356a8adbca2d87a20200629025758.jpg)

全选删除过程，把自动签名取消了在删除！

![](http://www.codetip.net/content/uploadfile/202006/4177cd4f1f542ae6067e292c18cd562020200629025758.jpg)

然后返回软件可以看到备份了一个和修改过的安装包，我们把刚刚修改过得安装包重命名为:libqingyan.so

![](http://www.codetip.net/content/uploadfile/202006/45797203e31d5675994e018bb251f88e20200629025758.jpg)

然后把备份的安装包还原！

![](http://www.codetip.net/content/uploadfile/202006/fc49d4039dbe06fe52ff87da6b6b32c120200629025758.jpg)

接着把libqingyan.so安装包移动到左边，然后打开备份的安装包/lib/armeabi-v7a/这个路径把它把左边的libqingyan.so文件添加进安装包，这时候把自动签名打钩！

![](http://www.codetip.net/content/uploadfile/202006/242be6203e8c2923cb0cc4d0951a6fb520200629025758.jpg)

然后我们返回软件根目录，打开加固的classes.dex文件，dex++方式打开

![](http://www.codetip.net/content/uploadfile/202006/9dad29103eb4f24f94e59cf324e00ea920200629025758.jpg)

依次点击com-stub-StubAPP

![](http://www.codetip.net/content/uploadfile/202006/79aa5db8fc9dc29489da435a8234d6d120200629025758.jpg)

接着点击右上角，选择搜索attachBaseContext

![](http://www.codetip.net/content/uploadfile/202006/a17476c8a99e6c384a7dc0f7d2f7345720200629025758.jpg)

下个第二个下面添加代码invoke-static {p0}, Lcom/qingyan/KillVerify/Stub360;->start(Landroid/content/Context;)V

![](http://www.codetip.net/content/uploadfile/202006/59780e3a78ef9c70168473a60169aaf520200629025758.jpg)

接着保存，一路返回并保存，取消自动签名更新

![](http://www.codetip.net/content/uploadfile/202006/f2639cb44e24aa4ac0a09368ae6e7ffd20200629025758.jpg)

然后我们把效验的文件夹解压出来，并把加固安装包的classes.dex文件移动到左边，接着选择两个dex文件然后合并

![](http://www.codetip.net/content/uploadfile/202006/8cf5d8b5c2df3d3edc19ae0a5f9387ca20200629025758.jpg)

合并之后，把classes.dex和classes2.dex原文件删除，合并的重命名为classes.dex

![](http://www.codetip.net/content/uploadfile/202006/139b046f4ff5e150d4b34b49b4055fce20200629025758.jpg)

接着把合并的classes.dex文件添加到安装包里面，直接覆盖并签名

![](http://www.codetip.net/content/uploadfile/202006/7766dda0b102defd021766ddaeb00fce20200629025758.jpg)

接着我们找到脱壳的文件，把脱壳的文件重命名为classes2.dex

![](http://www.codetip.net/content/uploadfile/202006/84be9d127b1db78ca03de1753ffc1f7920200629025758.jpg)

然后移动到加固安装包里面，打钩自动签名

![](http://www.codetip.net/content/uploadfile/202006/910a7879575248bd20edf37b2c25b45d20200629025758.jpg)

现在加固的安装包就有两个dex文件了

![](http://www.codetip.net/content/uploadfile/202006/182ac065919ff7cb13dbb5b1a251497a20200629025758.jpg)

这就是完整的360脱壳＋修复的教程了，然后返回安装包主界面，我们点击一下安装包，已经看到伪加固了

![](http://www.codetip.net/content/uploadfile/202006/467b14b7c210b67cdbf68ef2a2103c7320200629025758.jpg)

接下来，我们安装测试一下，然后打开开发者助手可以看到未加固的状态

![](http://www.codetip.net/content/uploadfile/202006/adfafa9d8e91d8c75b4344c8fe9f89c320200629025758.jpg)

到这里360脱壳＋修复的教程就结束了！感谢大家观看！这几天应该会更新全系的脱壳＋修复教程！感兴趣的可以点个关注喔！![斗鸡眼滑稽](http://www.codetip.net/content/uploadfile/202006/befc64d17151c9f9e8d4ab1ac66f3ffd20200629025759.png)![斗鸡眼滑稽](http://www.codetip.net/content/uploadfile/202006/befc64d17151c9f9e8d4ab1ac66f3ffd20200629025759.png)![斗鸡眼滑稽](http://www.codetip.net/content/uploadfile/202006/befc64d17151c9f9e8d4ab1ac66f3ffd20200629025759.png)  
  
所有文件都可以到这里下载:[查看链接](https://ldbk.lanzous.com/b0r7dlpi "https://ldbk.lanzous.com/b0r7dlpi")  