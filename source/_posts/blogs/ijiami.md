---
title: 安卓的脱壳之战-爱加密加固   
date: 2021-03-12 14:49:42
tags: 加固
keywords: 脱壳 
categories: [逆向]
---

日期：2020-6-29 

<!-- more -->
  
### 准备的软件如下:  

MT管理器+虚拟大师+反射大师  
  
安装包名为“某某输入法”

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606666000.jpg)

### 去虚拟大师里面导入并安装

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606702000.jpg)

### 打开反射大师，选择某某输入法打开

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606734000.jpg)

### 点击中间的图标脱出classes.dex

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606757000.jpg)

然后导出到本地，

### 在本地的MT管理器找到脱壳的dex并修复

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606781000.jpg)

然后把没有修复的删除，保留修复的文件重命名为classes.dex

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606744000.jpg)

接着我们找到加固的安装包，点击查看

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606773000.jpg)

老规矩用DEX++编辑器方式打开classes.dex文件

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606677000.jpg)

然后点击s.h.e.l.l，在点一次会出来一个N 和S的选项

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606633000.jpg)

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606697000.jpg)

我们点击S文件，然后搜索const-string v0

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606750000.jpg)

复制第二个入口com.moumou.input.MyApplication

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606788000.jpg)

然后返回软件主界面，我们点击AndroidManifest.xml，选择反编译

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606693000.jpg)

把入口复制到android:name那段代码修改前

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606683000.jpg)

修改后

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606740000.jpg)

右上角保存退出，取消自动签名确定

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606762000.jpg)

然后我们去删除残留文件，把这几个全部删完

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606671000.jpg)

然后把脱好的壳添加进去，自动签名

![](https://cdn.jsdelivr.net/gh/2926295173/cdn-picture@main/hpp/1615606689000.jpg)

这就脱壳+修复完成了，我们安装测试

![](http://www.codetip.net/content/uploadfile/202006/df1446c0de0e512d8653ef99b9619b0220200629030319.jpg)

可以看到未加固状态，开发者助手分析也是未知的

![](http://www.codetip.net/content/uploadfile/202006/8bbbd5f4aeeeb59a9729217e2037ea6a20200629030319.jpg)

好了，本次的爱加密脱壳教程就完了！