---
title: 给github pages加CDN
date: 2021-03-12 13:49:42
tags: [cdn,原创]
keywords: 加速
categories: [网站设计]
---
  自从用了github pages后发现国内访问比较慢，于是想给它加上个七牛云的cdn
   
<!-- more -->

以下是我的配置过程

1.在github 网页 的自定义域名处填写
www.aoian.cn
2.在dnspod处：
www.aoian.cn解析到2926295173.github.io
aoian.cn解析到七牛云的加速地址
3.七牛云配置
加速域名为aoian.cn
源站信息为2926295173.github.io
回源host为	www.aoian.cn
动态加速，配置完成！
