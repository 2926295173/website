---
title: 安卓的脱壳之战-邦邦脱壳   
date: 2021-03-13 13:49:42
tags: 加固
keywords: 脱壳
categories: [逆向]
---        		         
日期：2020-6-29  

那么前面已经讲了腾讯和360的加固了，不知道有多少小伙伴学会了，今天我们继续讲解邦邦加固的壳！  

<!-- more -->
  
废话不多说，开始今天的教程，这次准备的软件是在我亲戚家的小儿子上面看到的一款软件，名为:“小学语文数学英语同步”的APP！  
  
准备的软件依旧是:  
反射大师+MT管理器+虚拟大师  
  
首先在本地查看安装包，可以看到是梆梆加固

![](http://www.codetip.net/content/uploadfile/202006/a1d9cf9f23d90f75c3c5e395a3aeb72220200629030213.jpg)

接下来开始脱壳，脱壳的方法和360加固、腾讯加固一样，熟手直接下滑跳过这部分！我们打开虚拟大师，导入并安装需要脱壳的软件

![](http://www.codetip.net/content/uploadfile/202006/3d8cc792f2ea632fb3f1b19c4f5a1c2a20200629030213.jpg)

![](http://www.codetip.net/content/uploadfile/202006/9621ecff435ef001074f9fc37bf7100b20200629030213.jpg)

然后打开反射大师，选择这个软件并打开

![](http://www.codetip.net/content/uploadfile/202006/613c86dea2625af917a637ec6a7d79d320200629030213.jpg)

和之前一样，点击中间的图标，然后选择第二个选项，然后点击当前ACTIVITY，接着点击写出dex文件

![](http://www.codetip.net/content/uploadfile/202006/7f7403b850d1f38d1d2707cd115546eb20200629030213.jpg)

![](http://www.codetip.net/content/uploadfile/202006/9dc97ad63c0f648aec04e5aec497e6b320200629030214.jpg)

![](http://www.codetip.net/content/uploadfile/202006/14222f50bc9f9400175ea6b11901eb6c20200629030214.jpg)

然后导出到本地，虚拟大师就可以关闭了！

![](http://www.codetip.net/content/uploadfile/202006/d7c993289c6770d37e682a4671ec684f20200629030214.jpg)

接着MT管理器，找到VMOSfiletransferstation这个文件夹，把我们脱好的壳修复一下dex，然后删除未修复的，把修复好的重命名为classes.dex

![](http://www.codetip.net/content/uploadfile/202006/ba272e406be3bb6a1a6ff4585be899a220200629030214.jpg)

这就脱壳好了，接下来又到修复的时候了，我们找到加固的安装包查看，dex++编辑器classes.dex

![](http://www.codetip.net/content/uploadfile/202006/b791e1473b34e209807304e57c70829120200629030214.jpg)

然后依次点击com.SecShell.SecShell/Helper点击打开

![](http://www.codetip.net/content/uploadfile/202006/407ef23010c4d9dc8fbaf718bc307fb020200629030214.jpg)

然后搜索const-string，第二个地方复制下来

![](http://www.codetip.net/content/uploadfile/202006/eb24976a6bee08940e1243886de357bd20200629030214.jpg)

![](http://www.codetip.net/content/uploadfile/202006/3eaebed56542cb16a4b1fdf5b2818a9020200629030214.jpg)

然后退出到主界面选择AndroidManifest.xml，点击反编译打开

![](http://www.codetip.net/content/uploadfile/202006/4176cfccf5742f3d199e335952f3b7f820200629030214.jpg)

搜索<application

![](http://www.codetip.net/content/uploadfile/202006/4da523ba9e5c9082196a4ffa6a4c5cd120200629030214.jpg)

在android:name哪行把我们刚刚复制的粘贴进去，修改前

![](http://www.codetip.net/content/uploadfile/202006/3a9d58c926773efa35d200f33134066e20200629030214.jpg)

修改后

![](http://www.codetip.net/content/uploadfile/202006/944e58f6b3d076c4ef1586af864a64c620200629030214.jpg)

然后点击右上角保存，自动签名打钩确定

![](http://www.codetip.net/content/uploadfile/202006/86f346a87b18d0413ca848eb47b18b2c20200629030214.jpg)

接下来我们需要去删除残留文件点击assets，然后把SecShell0.jar这个文件删掉

![](http://www.codetip.net/content/uploadfile/202006/305fbc78cf8b46b9d1765b8004a5759920200629030214.jpg)

![](http://www.codetip.net/content/uploadfile/202006/686b920c9a5b3e4d715915c74379e45f20200629030215.jpg)

接着打开lib文件夹的armeabi，把里面的后三个SO删除

![](http://www.codetip.net/content/uploadfile/202006/b02bfa471b0c5800ae0ccaed4bec538120200629030215.jpg)

![](http://www.codetip.net/content/uploadfile/202006/e2979af5cf19f675c4800ff9b06bbed720200629030215.jpg)

然后回到主界面，替换classes.dex文件，把我们脱好的classes.dex添加进去

![](http://www.codetip.net/content/uploadfile/202006/ee9969627caa22c794adaae655dd8d6920200629030215.jpg)

然后然后安装包主界面，点击安装包

![](http://www.codetip.net/content/uploadfile/202006/a48060130948f7aef9c1f68c8ead331f20200629030215.jpg)

可以看到未加固了，我们接着安装测试

![](http://www.codetip.net/content/uploadfile/202006/434768a98c560439f43f2f791be5672720200629030215.jpg)

已经看到完美运行了，到这里梆梆加固的壳我们就干掉了，是不是也非常简单呢？

本博客所有文章如无特别注明均为原创。作者：[倪风](http://www.codetip.net/author/1) ，复制或转载请以超链接形式注明转自 [CodeTip技术博客](/) 。  
原文地址《安卓的脱壳之战-邦邦脱壳》


### 相关推荐

*   [安卓脱壳之战-360脱壳](http://www.codetip.net/post-18.html)
*   [安卓的脱壳之战-爱加密加固](http://www.codetip.net/post-21.html)
*   [安卓脱壳之战-腾讯脱壳](http://www.codetip.net/post-19.html)
