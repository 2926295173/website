---
title: 欢迎大家
date: 2021-02-05 13:49:42
tags: 公告
pin: true
music:
  server: tencent   # netease, tencent, kugou, xiami, baidu
  type: song        # song, playlist, album, search, artist
  id: 293826664      # song id / playlist id / album id / search keyword
  autoplay: false
---

 {% span center logo large, 欢迎大家 %}

十分欢迎大家的到来,小高博客历经了多次风雨，但是依然没有倒下...

我希望能够高质量的写好每一篇文章，不在数量.但求质量