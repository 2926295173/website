---
layout: page
title: 鸣谢项目和社区贡献者
cover: true
sidebar: []
music:
  server: netease   # netease, tencent, kugou, xiami, baidu
  type: song        # song, playlist, album, search, artist
  id: 1363205817      # song id / playlist id / album id / search keyword
  autoplay: true
---

小高的发展离不开各位的帮助,

与此同时，我们非常欢迎您的加入，在这里，可以发布你喜欢的文章。

让我们一同成长。


<!-- more -->


## 感谢主题开发者

{% btns circle grid5 %}
{% cell MHuiG, https://github.com/MHuiG, https://avatars.githubusercontent.com/u/38281581?s=60&v=4 %}
{% cell 查看全部, https://github.com/volantis-x/hexo-theme-volantis/graphs/contributors, fab fa-github %}
{% endbtns %}


## 感谢社区建设者

{% btns circle grid5 %}
{% cell xaoxuu, https://github.com/xaoxuu, https://avatars.githubusercontent.com/u/16400144?s=60&v=4 %}
{% cell MHuiG, https://github.com/MHuiG, https://avatars.githubusercontent.com/u/38281581?s=60&v=4 %}
{% endbtns %}

> 参与社区建设的朋友

## 如何参与社区建设

社区建设主要包括 {% btn, Issues, https://github.com/volantis-x/hexo-theme-volantis/issues %} {% btn, Discussions(论坛), https://github.com/volantis-x/hexo-theme-volantis/discussions %} {% btn, 官网博客收录, #如何收录博客 %} {% btn, 官网文档维护, #如何维护文档 %} 几个方面。


### 发布文章注意
如果这篇文章的作者是第一次出现在官网，还需要在 `_data/author.yml` 文件中添加作者信息，例如：

```yaml _data/author.yml
...
inkss:
  name: 枋柚梓
  avatar: https://cdn.jsdelivr.net/gh/inkss/common@1.4.2/hexo/img/static/avatar.jpg
  url: https://inkss.cn
```

{% noteblock warning %}
**注意事项**
文章存放在 `_posts/blogs/` 目录中，且文件名格式为「年-月-日-文章话题」。
{% endnoteblock %}


{% link 在线编辑文档, https://github.com/volantis-x/community/tree/master/source/_posts/blogs %}
