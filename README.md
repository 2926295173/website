# 我的小网站[![deepcode](https://www.deepcode.ai/api/gh/badge?key=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwbGF0Zm9ybTEiOiJnaCIsIm93bmVyMSI6IjI5MjYyOTUxNzMiLCJyZXBvMSI6IndlYnNpdGUiLCJpbmNsdWRlTGludCI6ZmFsc2UsImF1dGhvcklkIjoyNzMzMiwiaWF0IjoxNjE3MzY5NTgxfQ.zCwerLrGw2-D3NlVyJzcCErWSaKEShjeT94k0a67VUY)](https://www.deepcode.ai/app/gh/2926295173/website/_/dashboard?utm_content=gh%2F2926295173%2Fwebsite)

Cloudflare Pages: https://website-e8v6.pages.dev/
[小高导航](https://go.aoian.cn)
vercel: https://website-2926295173.vercel.app

主题[社区主页](https://volantis.js.org)

## 主题文档索引

中文文档：
[v4](https://volantis.js.org/getting-started/)



## 我的网站展示源码

[小高展示源](https://github.com/2926295173/2926295173.github.io)

[网站设置源](https://github.com/2926295173/website)

[查看主题更新](https://github.com/2926295173/hexo-theme-volantis)

## 帮助


基于腾讯云的网页编辑环境：https://aoian5173.cloudstudio.net/ws/rvxiia

基于hexoplusplus的在线编辑：https://hpp.aoian5173.workers.dev/

[图片cdn](https://github.com/2926295173/cdn-picture)
